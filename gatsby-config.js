module.exports = {
  siteMetadata: {
    title: 'Radish Creative Lab',
    description: 'We deliver cutting-edge experiences'
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'Radish Creative Lab',
        short_name: 'Radish',
        start_url: '/',
        background_color: '#ffffff',
        theme_color: '#ffffff',
        display: 'minimal-ui',
        icon: 'src/images/radish-icon.png'
      }
    },
    'gatsby-plugin-offline',
    'gatsby-plugin-react-svg'
  ]
};
