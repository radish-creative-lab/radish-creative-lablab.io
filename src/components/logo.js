import React from 'react';
import { Link } from 'gatsby';
import Logo from '../images/logo.svg';

export default () => (
  <Link to="/">
    <Logo />
  </Link>
);
